<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1.0"/>
	<title>Contact</title>
	<link href="css/skin.css" rel="stylesheet" type="text/css">
</head>
<body>
	<form action="recup.php" method="POST">
		<div class="container">
			<div class=""><h1>CONTACT</h1></div>
			<div class="logo"><img src="images/LogoBulko.png" alt="Logo"></div>
			<div class="rs">
				<a href="https://www.facebook.com/bulko.net/"><img src="images/RS1.png" alt="RS1"></a>
				<a href="https://www.linkedin.com/company/bulko/"><img src="images/RS2.png" alt="RS2"></a>
			</div>
			<div class="box"></div>
			<div class="info">
				<div class="nom">
					<input class="textbox" type = "text" placeholder="Nom" name="nom" id="nom" required="required" pattern="[a-zA-Z.-$]">
				</div>
				<div class="mail">
					<input class="textbox" type = "text" placeholder="Mail" name="mail" id="mail" required="required" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" title="Veuillez fournir une adresse e-mail correcte.">
				</div>
				<div class="tel">
					<input class="textbox" type = "text" placeholder="Téléphone" name="tel" id="tel" required="required" pattern="0+[0-9]{9}" title="Veuillez fournir un numéro de téléphone correct.">
				</div>
				<textarea class="textbox" placeholder="Message" name="msg" id="msg" required="required" rows="5" cols="10"></textarea>
			</div>
			<div class="envoi">
				<input type="image" name="submit" src="images/BoutonEnvoyer.png" alt="Envoi"/>
			</div>
		</div>
	</form>
</body>
</html>